FROM registry.gitlab.com/mrponpon/ocr-do-ai:env_base
WORKDIR /app
COPY . .
CMD uvicorn app.main:app --host "0.0.0.0" --port "5000" --workers 4 --reload --debug
