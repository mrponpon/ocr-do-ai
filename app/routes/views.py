from fastapi import APIRouter, Depends, Request
from app.apis.predict_tgp.predict_ai import predict_image as predict_tgp_img
from app.apis.predict_weber.predict_ai import predict_image as predict_weber_img
from app.apis.healthcheck.health_check import healthChecking
from pydantic import BaseModel
from starlette.responses import JSONResponse

class Item(BaseModel):
    img: str

router = APIRouter()

@router.get("/")
def index():
    return "HELLO OCR API"

@router.get("/api/v1/health-check")
def view_1(request: Request):
    return JSONResponse(content = healthChecking(request), status_code = 200)

@router.post("/api/v1/tgp/predict", tags=["api_predict"])
def view_2(request: Request,img: Item):
    return JSONResponse(content = predict_tgp_img(request,img), status_code = 200)

@router.post("/api/v1/weber/predict", tags=["api_predict"])
def view_3(request: Request,img: Item):
    return JSONResponse(content = predict_weber_img(request,img), status_code = 200)