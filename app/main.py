from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware
from app.routes import views
from app.utils import label_map_util_lite
from app.utils.class_models import Openvino_TF
import openvino.runtime as ov
import os
import time
import urllib3
urllib3.disable_warnings()
app = FastAPI()
print(os.getcwd())

@app.on_event("startup")
def compiling_models():
    app.ir_tgp_dataset_v2 = Openvino_TF(f'{os.getcwd()}/app/models/ir_DO_TGP/saved_model.xml',f'{os.getcwd()}/app/models/ir_DO_TGP/labelmap.pbtxt')
    app.ir_weber_dataset_v2 = Openvino_TF(f'{os.getcwd()}/app/models/ir_DO_WEBER/saved_model.xml',f'{os.getcwd()}/app/models/ir_DO_WEBER/labelmap.pbtxt')

# Set all CORS enabled origins
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(views.router)