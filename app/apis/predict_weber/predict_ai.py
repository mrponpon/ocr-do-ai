import sys
import os
import numpy as np
import base64
import json
import cv2
import time
import datetime
from app.utils import visualization_utils_lite as vis_util
from app.utils import extract_text
from multiprocessing.pool import ThreadPool
pool = ThreadPool(processes=5)

def predict_image(request, img):
    ir_weber_dataset_v2 = request.app.ir_weber_dataset_v2
    date_time = datetime.datetime.now()
    print(f'PREDICT DATE: {date_time}')
    image = base64.b64decode(img.img)
    npimg = np.frombuffer(image, np.uint8)
    image = cv2.imdecode(npimg,cv2.IMREAD_COLOR)
    height, width,_ = image.shape
    (boxes, scores, classes) = ir_weber_dataset_v2.detect(image)
    _ ,str_box_map  = vis_util.visualize_boxes_and_labels_on_image_array(
                    image,
                    boxes,
                    classes,
                    scores,
                    ir_weber_dataset_v2.category_index,
                    use_normalized_coordinates=True,
                    max_boxes_to_draw = ir_weber_dataset_v2.num_classes,
                    line_thickness=1,
                    min_score_thresh=0.5,
                    draw_boxes=False)
    
    result_dict = {'no':''}
    worker_dict = {}
    for box,label_string in str_box_map.items():
        label_string = label_string[0].split(':')[0]
        worker_dict[label_string] = pool.apply_async(extract_text.extracter,(image,box,label_string,height, width,))
    for k,v in worker_dict.items():
        result_dict[k] = v.get()

    return result_dict
    



    


    
    

