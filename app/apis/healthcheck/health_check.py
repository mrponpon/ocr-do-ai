import numpy as np
import cv2

def healthChecking(request):
    print("health checking..")
    image = np.random.randint(255, size=(100,100,3),dtype=np.uint8)
    request.app.ir_weber_dataset_v2.detect(image)
    request.app.ir_tgp_dataset_v2.detect(image)

    return {"message":"SUCCESS."}