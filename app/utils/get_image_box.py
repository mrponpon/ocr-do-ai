import cv2

import numpy as np

import math

from PIL import Image


def compute_ratio_and_resize(img,width,height,model_height):
    '''

    Calculate ratio and resize correctly for both horizontal text

    and vertical case

    '''
    ratio = width/height

    if ratio<1.0:

        ratio = calculate_ratio(width,height)

        img = cv2.resize(img,(model_height,int(model_height*ratio)), interpolation=Image.ANTIALIAS)

    else:

        img = cv2.resize(img,(int(model_height*ratio),model_height),interpolation=Image.ANTIALIAS)

    return img,ratio


def calculate_ratio(width,height):
    '''

    Calculate aspect ratio for normal use case (w>h) and vertical text (h>w)

    '''

    ratio = width/height

    if ratio<1.0:

        ratio = 1./ratio

    return ratio


def four_point_transform(image, rect):
    (tl, tr, br, bl) = rect

    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))

    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))

    maxWidth = max(int(widthA), int(widthB))


    # compute the height of the new image, which will be the

    # maximum distance between the top-right and bottom-right

    # y-coordinates or the top-left and bottom-left y-coordinates

    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))

    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))

    maxHeight = max(int(heightA), int(heightB))


    dst = np.array([[0, 0],[maxWidth - 1, 0],[maxWidth - 1, maxHeight - 1],[0, maxHeight - 1]], dtype = "float32")


    # compute the perspective transform matrix and then apply it

    M = cv2.getPerspectiveTransform(rect, dst)

    warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))
    return warped


def get_image_list(horizontal_list, free_list, img, model_height = 64, sort="top-to-bottom"):

    image_list = []

    maximum_y,maximum_x,_ = img.shape

    max_ratio_hori, max_ratio_free = 1,1
    for box in free_list:
        rect = np.array(box, dtype = "float32")
        transformed_img = four_point_transform(img, rect)
        ratio = calculate_ratio(transformed_img.shape[1],transformed_img.shape[0])
        new_width = int(model_height*ratio)
        if new_width == 0:
            pass
        else:
            crop_img,ratio = compute_ratio_and_resize(transformed_img,transformed_img.shape[1],transformed_img.shape[0],model_height)
            image_list.append( (box,crop_img) ) # box = [[x1,y1],[x2,y2],[x3,y3],[x4,y4]]
            max_ratio_free = max(ratio, max_ratio_free)

    max_ratio_free = math.ceil(max_ratio_free)
    
    for box in horizontal_list:

        x_min = max(0,box[0])

        x_max = min(box[1],maximum_x)

        y_min = max(0,box[2])

        y_max = min(box[3],maximum_y)

        crop_img = img[y_min : y_max, x_min:x_max]
        width = x_max - x_min
        height = y_max - y_min
        ratio = calculate_ratio(width,height)
        new_width = int(model_height*ratio)
        if new_width == 0:
            pass

        else:
            crop_img,ratio = compute_ratio_and_resize(crop_img,width,height,model_height)

            image_list.append( ( [[x_min,y_min],[x_max,y_min],[x_max,y_max],[x_min,y_max]] ,crop_img) )


    if sort == "top-to-bottom":

        image_list = sorted(image_list, key=lambda item: item[0][0][1])

    if sort == "left-to-right":

        image_list = sorted(image_list, key=lambda item: item[0][0][0]) 

    return image_list

