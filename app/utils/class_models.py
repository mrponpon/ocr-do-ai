from app.utils import label_map_util_lite
import openvino.runtime as ov
from openvino.inference_engine import IECore
import os
import numpy as np
import cv2

class Openvino_TF():
    def __init__(self,model_xml,labelmap_path):
        self.core = ov.Core()
        self.compiled_model = self.core.compile_model(self.core.read_model(model_xml), "CPU")
        self.input_layer = self.compiled_model.input(0)
        self.category_index = label_map_util_lite.create_category_index_from_labelmap(labelmap_path, use_display_name=True)
        self.num_classes = max(item[0] for item in self.category_index.items())

    @property
    def get_input_shape(self):
        N, H, W, C = self.input_layer.shape
        return (N, H, W, C)

    @property
    def get_height_width(self):
        _, H, W, _ = self.get_input_shape
        return (H, W)

    def detect(self,image):
        N, H, W, C = self.get_input_shape
        # print(N, C, H, W )
        resized_image = cv2.resize(src=image, dsize=(W, H))
        # print(resized_image.shape)
        input_data = np.expand_dims(np.transpose(resized_image, (0, 1, 2)), 0).astype(np.float32)
        # print(input_data.shape)
        request =  self.compiled_model.create_infer_request()
        request.infer(inputs={self.input_layer.any_name: input_data})
        scores = request.get_output_tensor(1).data
        boxes = request.get_output_tensor(5).data
        classes = request.get_output_tensor(6).data
        return boxes[0], scores, classes[0]