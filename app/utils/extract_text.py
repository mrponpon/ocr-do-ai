import base64
import time
import datetime
import re
import requests
import json
import os
from PIL import Image
import cv2
import ast
import numpy as np
from .get_image_box import get_image_list
import pytesseract


ip_api_ocr = os.getenv("api_ocr",default = None)
manageai_key = os.getenv("manageai_key",default = None)
use_tesseract = eval(os.getenv("use_tesseract",default = "False"))
tap_right_percent = os.getenv("tap_right_percent",default = 10)
tap_left_percent = os.getenv("tap_left_percent",default = 0)
print(ip_api_ocr,manageai_key,use_tesseract)

def regex_letterTH(text):
    match = re.sub(r"[^\u0E00-\u0E7F']", '', text)
    if match == None:
        return ""
    return match

def regex_letterEN(text):
    match = re.sub(r'[^A-Za-z]', '', text)
    if match == None:
        return ""
    return match

def regex_number_letter(text):
    match = re.sub(r'[^A-Za-z0-9_/-]', '', text)
    if match == None:
        return ""
    return match

def regex_date(text):
    match = re.search(r'(\d+/\d+/\d+)',text)
    if match == None:
        return ""
    match = match.group(1)
    return match

def regex_number(text):
    match = re.sub('[^0-9]()', '', text)
    if match == None:
        return ""
    return match

def regex_price(text):
    match = re.sub('[^0-9,.]', '', text)
    if match == None:
        return ""
    return match

def normalized_coor(xmin,xmax,ymin,ymax,im_width,im_height):
    (left, right, top, bottom) = (xmin * im_width, xmax * im_width,
                                ymin * im_height, ymax * im_height)
    return int(left), int(right), int(top), int(bottom)

def get_box(img):
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    (thresh, img_bin) = cv2.threshold(img, 128, 255,cv2.THRESH_BINARY | cv2.THRESH_OTSU)  # Thresholding the image
    img_bin = 255-img  # Invert the image
    kernel_length = np.array(img).shape[1]//40
    verticle_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (1, kernel_length))
    hori_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (kernel_length, 1))
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
    img_temp1 = cv2.erode(img_bin, verticle_kernel, iterations=3)
    verticle_lines_img = cv2.dilate(img_temp1, verticle_kernel, iterations=3)
    # cv2.imwrite("testing/verticle_lines.jpg",verticle_lines_img)
    img_temp2 = cv2.erode(img_bin, hori_kernel, iterations=3)
    horizontal_lines_img = cv2.dilate(img_temp2, hori_kernel, iterations=3)
    # cv2.imwrite("testing/horizontal_lines.jpg",horizontal_lines_img)
    alpha = 0.5
    beta = 1.0 - alpha
    img_final_bin = cv2.addWeighted(verticle_lines_img, alpha, horizontal_lines_img, beta, 0.0)
    img_final_bin = cv2.erode(~img_final_bin, kernel, iterations=2)
    (thresh, img_final_bin) = cv2.threshold(img_final_bin, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
    # cv2.imwrite("testing/img_final_bin.jpg",img_final_bin)
    contours, hierarchy = cv2.findContours(img_final_bin, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    # print(contours)
    return contours

def pre_auto_row(img, morph_size=(3,3), threshold=4):
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    threshed = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
    cpy = threshed.copy()
    struct = cv2.getStructuringElement(cv2.MORPH_RECT, morph_size)
    cpy = cv2.dilate(~cpy, struct, anchor=(-1, -1), iterations=1)
    hist = cv2.reduce(cpy,1, cv2.REDUCE_AVG).reshape(-1)
    th = threshold
    H,W = img.shape[:2]
    lowers = [y for y in range(H-1) if hist[y]>th and hist[y+1]<=th]
    return H,W,lowers

def sort_contours(cnts, method="left-to-right"):
    reverse = False
    i = 0
    if method == "right-to-left" or method == "bottom-to-top":
        reverse = True
    if method == "top-to-bottom" or method == "bottom-to-top":
        i = 1
    boundingBoxes = [cv2.boundingRect(c) for c in cnts]
    (cnts, boundingBoxes) = zip(*sorted(zip(cnts, boundingBoxes),
                                        key=lambda b: b[1][i], reverse=reverse))
    return (cnts, boundingBoxes)


def extract_address(crop_img,text_threshold,link_threshold,is_bbox):
    result_list1 = []
    H,W,row_line = pre_auto_row(crop_img)
    vis = crop_img.copy()
    # cv2.line(vis, (0,0), (W,0), (0,0,0), 1)
    for y in row_line:
        cv2.line(vis, (0,y+2), (W, y+2), (0,0,0), 1)
    cv2.rectangle(vis,(0,0),(W,H),(0,0,0), 1)
    # cv2.imwrite(f'testing/generate_horline_address.jpg',vis)
    contours = get_box(vis)
    (contours, boundingBoxes) = sort_contours(contours, method="top-to-bottom")
    n = 0
    for contour in contours[:len(contours)]:
        n += 1
        area = cv2.contourArea(contour)
        if area > 5000:
            x, y, w, h = cv2.boundingRect(contour)
            new_img = crop_img[y-5:y+h+5, x-2:x+w+2]
            # cv2.imwrite(f"testing/address_{n}.jpg",new_img)
            text = ocr_topsarun_api_longtext(new_img,text_threshold= text_threshold,link_threshold=link_threshold,is_bbox=False)
            # print(text)
            if text:
                result_list1.append(text)
    return result_list1

def ocr_api(image):
    retval, buffer = cv2.imencode('.jpg', image)
    img_base64 = base64.b64encode(buffer).decode('utf-8')
    headers = {"manageai-key":str(manageai_key)}
    data = {
        "base64img": img_base64
    }
    r = requests.post(str(ip_api_ocr), json = data,headers=headers,verify=False)
    result = r.json()
    if result !=None:
        return result["prediction"][0]["text"]
    else:
        return ""
        
def tesseract(image, _lang="tha+eng", mode='11'):
    return pytesseract.image_to_string(
        image,
        lang=_lang,
        config=f'--psm {mode} --oem 3 --tessdata-dir "{os.getcwd()}/app/utils/lang"'
    )

def normalized_coor(xmin,xmax,ymin,ymax,im_width,im_height):
    (left, right, top, bottom) = (xmin * im_width, xmax * im_width,
                                ymin * im_height, ymax * im_height)
    return int(left), int(right), int(top), int(bottom)
def resize_box(crop_img,ratio):
    model_height = 64
    crop_img = cv2.resize(crop_img, (int(model_height*ratio), model_height), interpolation =  Image.ANTIALIAS)
    return crop_img

def extracter(image_copy,box,label_string,height, width):
    
    ymin, xmin, ymax, xmax = box
    left, right, top, bottom = normalized_coor(xmin,xmax,ymin,ymax,width,height)
    width = right - left 
    height = bottom - top
    ratio = width/height
    tap_right = int(width*int(tap_right_percent)/100)
    tap_left = int(width*int(tap_left_percent)/100)

    try:
        crop_img = image_copy[int(top):int(bottom), max(int(left)-tap_left,0):int(right)+tap_right]
        # cv2.imwrite(f"testing/{str(label_string)}.jpg",crop_img)
    except:
        crop_img = image_copy[int(top):int(bottom), int(left):int(right)]
        # cv2.imwrite(f"testing/{str(label_string)}.jpg",crop_img)
    if use_tesseract:
        crop_img = remove_line_table(crop_img)
        # crop_img = resize_box(crop_img,ratio)
        # cv2.imwrite(f'img_revomeline.jpg',crop_img)
        text = tesseract(crop_img, _lang="eng", mode='11')
    else:
        text = ocr_api(crop_img)
    text = regex_number(text)
    return text

def resizer(image):
    if  image.shape[0] > 3800 or image.shape[1] > 3800:
        image = cv2.resize(image, (int(image.shape[1]/2), int(image.shape[0]/2)))
        logging.info("after_reshape %s,%s" %(image.shape[0],image.shape[1]))

    elif  image.shape[0] > 2000 or image.shape[1] > 2000:
        image = cv2.resize(image, (int(image.shape[1]/1.5), int(image.shape[0]/1.5)))
        logging.info("after_reshape %s,%s" %(image.shape[0],image.shape[1]))
    return image

def remove_line_table(image,remove_hor=True,remove_ver=True):
    if len(image.shape) == 3:
        image = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
    #THRESH
    result = image.copy()
    H,W = result.shape[:2]
    thresh = cv2.threshold(result, 240, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]
    # cv2.imwrite('thresh.jpg',thresh)

    # Remove horizontal lines
    if remove_hor:
        horizontal_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (40,1))
        remove_horizontal = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, horizontal_kernel, iterations=2)
        cnts = cv2.findContours(remove_horizontal, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnts = cnts[0] if len(cnts) == 2 else cnts[1]
        for c in cnts:
            cv2.drawContours(result, [c], -1, (255,255,255), 1)
    else:
        horizontal_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (40,1))
        remove_horizontal = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, horizontal_kernel, iterations=2)
        cnts = cv2.findContours(remove_horizontal, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnts = cnts[0] if len(cnts) == 2 else cnts[1]
        for c in cnts:
            cv2.drawContours(result, [c], -1, (0,0,0), 1)

    #Remove vertical lines
    if remove_ver:
        vertical_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (1,40))
        remove_vertical = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, vertical_kernel, iterations=2)
        cnts = cv2.findContours(remove_vertical, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnts = cnts[0] if len(cnts) == 2 else cnts[1]
        for c in cnts:
            cv2.drawContours(result, [c], -1, (255,255,255), 1)
    else:
        vertical_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (1,40))
        remove_vertical = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, vertical_kernel, iterations=2)
        cnts = cv2.findContours(remove_vertical, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnts = cnts[0] if len(cnts) == 2 else cnts[1]
        for c in cnts:
            x,y,w,h = cv2.boundingRect(c)
            cv2.rectangle(result, (x, 0), (x + w,H), (0,0,0), 1)
            # cv2.drawContours(result, [c], -1, (36,255,12), 2)
    return result

def rgb2gray3d(img):
    img = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    return img